var activeModules = [

    {name: 'module 1'},
    {name: 'module 2'},
    {name: 'module 11'},
    {name: 'module 3'},
    {name: 'module 10'}

];
// ogólnie troche mi to czasu zajeło bo nie wszystko wiedziałem i musiałem troche o tym poczytać
// na początku chciałem tylko dodać wcięcia i odstępy między liniami kodu ale to by było chyba troche za mało
// mam nadzieje, że nie zmieniłem funkcjonalności kodu
var getCustomModuleNumber = function() {

	var max = 0,
			customModuleRe = new RegExp('\\d+');

	activeModules.map(function(moduleName){

		current = parseInt(customModuleRe.exec(moduleName.name));

  	if(current > max){

				max = current

			}

  	});

	return max;

};

console.log(getCustomModuleNumber());
